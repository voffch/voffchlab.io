"use strict";

var chemCalc = (function() {

//atomic weight sources:
//https://www.ciaaw.org - CIAAW
//https://doi.org/10.1515/pac-2019-0603 - IUPAC, 2021
//https://doi.org/10.1515/pac-2017-1002 - IUPAC recommendations, 2021
var periodicTable = {
    H : 1.008,
    He : 4.002602,
    Li : 6.94,
    Be : 9.0121831,
    B : 10.81,
    C : 12.011,
    N : 14.007,
    O : 15.9994,
    F : 18.998403162,
    Ne : 20.1797,
    Na : 22.98976928,
    Mg : 24.305,
    Al : 26.9815384,
    Si : 28.085,
    P : 30.973761998,
    S : 32.06,
    Cl : 35.45,
    Ar : 39.95,
    K : 39.0983,
    Ca : 40.078,
    Sc : 44.955907,
    Ti : 47.867,
    V : 50.9415,
    Cr : 51.9961,
    Mn : 54.938043,
    Fe : 55.845,
    Co : 58.933194,
    Ni : 58.6934,
    Cu : 63.546,
    Zn : 65.38,
    Ga : 69.723,
    Ge : 72.63,
    As : 74.921595,
    Se : 78.971,
    Br : 79.904,
    Kr : 83.798,
    Rb : 85.4678,
    Sr : 87.62,
    Y : 88.905838,
    Zr : 91.224,
    Nb : 92.90637,
    Mo : 95.95,
    Tc : 96.90636,
    Ru : 101.07,
    Rh : 102.90549,
    Pd : 106.42,
    Ag : 107.8682,
    Cd : 112.414,
    In : 114.818,
    Sn : 118.71,
    Sb : 121.76,
    Te : 127.6,
    I : 126.90447,
    Xe : 131.293,
    Cs : 132.90545196,
    Ba : 137.327,
    La : 138.90547,
    Ce : 140.116,
    Pr : 140.90766,
    Nd : 144.242,
    Pm : 144.91276,
    Sm : 150.36,
    Eu : 151.964,
    Gd : 157.25,
    Tb : 158.925354,
    Dy : 162.5,
    Ho : 164.930329,
    Er : 167.259,
    Tm : 168.934219,
    Yb : 173.045,
    Lu : 174.9668,
    Hf : 178.486,
    Ta : 180.94788,
    W : 183.84,
    Re : 186.207,
    Os : 190.23,
    Ir : 192.217,
    Pt : 195.084,
    Au : 196.96657,
    Hg : 200.592,
    Tl : 204.3835,
    Pb : 207.2,
    Bi : 208.9804,
    Po : 208.98243,
    At : 209.98715,
    Rn : 222.01758,
    Fr : 223.01973,
    Ra : 226.02541,
    Ac : 227.02775,
    Th : 232.0377,
    Pa : 231.03588,
    U : 238.02891,
    Np : 237.04817,
    Pu : 244.0642,
    Am : 243.06138,
    Cm : 247.07035,
    Bk : 247.07031,
    Cf : 251.07959,
    Es : 252.08298,
    Fm : 257.09511,
    Md : 258.09843,
    No : 259.101,
    Lr : 262.10962,
    Rf : 267.12179,
    Db : 268.12567,
    Sg : 269.1285,
    Bh : 270.13337,
    Hs : 269.13365,
    Mt : 277.15353,
    Ds : 281.16455,
    Rg : 282.16934,
    Cn : 285.17723,
    Nh : 286.18246,
    Fl : 290.19188,
    Mc : 290.19624,
    Lv : 293.20458,
    Ts : 294.21084,
    Og : 294.21398
};

function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop)) {
        return false;
        }
    }
    return JSON.stringify(obj) === JSON.stringify({});
}

var chemRegExp = (function () {
    var leftBracket = "[\\u0028\\u005B\\u007B]";// ({[
    var rightBracket = "[\\u0029\\u005D\\u007D]";// )}]
    var cdot = "[\\u002A\\u002B\\u00B7]";// +*cdot
    var element = "[A-Z][a-z]*";
    var num = "[0-9]*\\.?[0-9]+|[0-9]+\\.?[0-9]*";
    var formula = [element, num, leftBracket, rightBracket, cdot].join("|");
    var equation = "[^\\s\\+=]+|=";
    
    var regExps = {
        "leftBracket" : RegExp(leftBracket),
        "rightBracket" : RegExp(rightBracket),
        "cdot" : RegExp(cdot),
        "element" : RegExp(element),
        "number" : RegExp(num),
        "formula" : RegExp(formula, "g"),
        "equation" : RegExp(equation, "g")
    }

    function result(what) {
        return regExps[what];
    }

    return result;
})();

function ChemicalSubstance(formulaText) {
    this.formula = formulaText;
    this.html = "";
    this.elementMoles = {};
    this.molarMass = 0;
    this.valid = false;
    this.startsWithNumber = false;
    this.init = function(formula) {
        function FormulaToken(value, type) {
            //just a token type definition
            this.value = value;//text or number
            this.type = type;//unknown, cdot, element, quantity, leftBracket, rightBracket
        }

        function tokenize(strInput) {
            //takes string as an input
            //returns an array of FormulaToken tokens
            //thanks to regexps, this one is pretty straightforward
            var tokenArray = [];
            if (strInput.length > 0) {
                var tokenStringArray = strInput.match(chemRegExp("formula"));
                if (tokenStringArray != null && tokenStringArray.length > 0) {
                    for (var tokenText of tokenStringArray) {
                        var thisToken;
                        if (chemRegExp("element").test(tokenText)) {
                            if (typeof(periodicTable[tokenText]) != "undefined") {
                                thisToken = new FormulaToken(tokenText, "element");
                            } else {
                                thisToken = new FormulaToken(tokenText, "unknown");
                            }
                        } else if (chemRegExp("number").test(tokenText)) {
                            thisToken = new FormulaToken(Number(tokenText), "quantity");
                        } else if (chemRegExp("leftBracket").test(tokenText)) {
                            thisToken = new FormulaToken(tokenText, "leftBracket");
                        } else if (chemRegExp("rightBracket").test(tokenText)) {
                            thisToken = new FormulaToken(tokenText, "rightBracket");
                        } else if (chemRegExp("cdot").test(tokenText)) {
                            thisToken = new FormulaToken(tokenText, "cdot");
                        } else {
                            //not quite accessible
                            thisToken = new FormulaToken("???", "unknown");
                        }
                        tokenArray.push(thisToken);
                    }
                }
            }
            return tokenArray;
        }

        function appendElement(elementsObject, element, quantity) {
            //adds certain amount of element to the elementsObject
            if (typeof(elementsObject[element]) == "undefined") {
                elementsObject[element] = quantity;
            } else {
                elementsObject[element] += quantity;
            }
        }

        function countElements(tokenArray) {
            //this is where the most important part of the whole rigmarole happens
            //complicated 'cause the actual formula may look something like this 2(3Cu((SO4.5)3CO3)2)5*2.8H2O

            function multiplyQuantitiesByNumber(elementsObject, num) {
                for (var element in elementsObject) {
                    elementsObject[element] *= num;
                }
            }

            //here the elements will be located
            var formulaElements = {};
            //formulaParts are the ones separated by * (e.g. CuSO4 and 5H2O in CuSO4*5H2O)
            //an array of objects whose indices are chemical element symbols and values are element quantities
            var formulaParts = [];
            formulaParts.push({});
            var formulaPartIndex = 0;
            //an array is essential for multiple-bracketed formulae
            var multiplyingCoeffArray = [];
            var currentMultiplier = 1;
            //starting from right to left
            for (var i = tokenArray.length - 1; i >= 0; i--) {
                switch (tokenArray[i].type) {
                    case "quantity":
                        //we place the numbers encountered in currentMultiplier first
                        //the next token determines what to do with it
                        currentMultiplier = tokenArray[i].value;
                        break;
                    case "rightBracket":
                        //until the next leftBracket, the elements' quantities
                        //should be multiplied by the currentMultiplier
                        //that's why we push it
                        multiplyingCoeffArray.push(currentMultiplier);
                        currentMultiplier = 1;
                        break;
                    case "leftBracket":
                        multiplyingCoeffArray.pop();
                        if (i < (tokenArray.length - 1) && tokenArray[i+1].type != "quantity") {
                            break;
                        } else {
                            //left bracket encountered before (seen left-to-right) the number is a special case
                            //and is kinda equivalent to the next cdot one
                            //hence no break here
                            //break;
                        }
                    case "cdot":
                        if (currentMultiplier != 1) {
                            if (isEmpty(formulaParts[formulaPartIndex]) && formulaPartIndex > 0) {
                                //another special case of quantity stuck between * and (
                                //like in H2O*2(3CO2)
                                //here the quantity "2" applies to the previous formulaPart
                                multiplyQuantitiesByNumber(formulaParts[formulaPartIndex-1], currentMultiplier);
                            } else {
                                multiplyQuantitiesByNumber(formulaParts[formulaPartIndex], currentMultiplier);
                            }
                            currentMultiplier = 1;
                        }
                        //init the next formulaPart
                        formulaParts.push({});
                        formulaPartIndex += 1;
                        break;
                    case "element":
                        //works the same regardless of whether encountered after the number or not
                        for (var multiplier of multiplyingCoeffArray) {
                            currentMultiplier *= multiplier;
                        }
                        appendElement(formulaParts[formulaPartIndex], tokenArray[i].value, currentMultiplier);
                        currentMultiplier = 1;
                        break;
                    case "unknown":
                    default:
                        break;
                }
            }
            if (currentMultiplier != 1) {
                //numeric multiplier preceding the last formulaPart
                //means that we have to increase the quantity of all the elements in it
                if (isEmpty(formulaParts[formulaPartIndex]) && formulaPartIndex > 0) {
                    //that special case described above still applies!
                    multiplyQuantitiesByNumber(formulaParts[formulaPartIndex-1], currentMultiplier);
                } else {
                    multiplyQuantitiesByNumber(formulaParts[formulaPartIndex], currentMultiplier);
                }
            }
            //now we need to consolidate all the formulaParts into formulaElements
            //but before we do that, let's init the formulaElements
            //so as to put all the elements in order of appearance
            for (var token of tokenArray) {
                if (token.type == "element") {
                    appendElement(formulaElements, token.value, 0);
                }
            }
            for (var part of formulaParts) {
                for (var element in part) {
                    appendElement(formulaElements, element, part[element]);
                }
            }
            return formulaElements;
        }

        function validateFormula(tokenArray) {
            var errorMsg = "";
            var brackets = 0;
            //check the logic of the token placement in the formula
            //try to catch at least some of the most obvious inconvenient mistakes
            try {
                if (tokenArray.length <= 0) {
                    throw "empty formula";
                }
                for (var i = 0; i < tokenArray.length; i++) {
                    switch (tokenArray[i].type) {
                        case "quantity":
                            if (i == 0) {
                                if (tokenArray.length <= 1) {
                                    throw "no elements found";
                                } else if (tokenArray[i+1].type == "cdot") {
                                    throw "begins with " + tokenArray[i].value.toString() + " which refers to nothing";
                                }
                            } else if (i == (tokenArray.length - 1)) {
                                if (tokenArray[i-1].type == "cdot") {
                                    throw "ends with " + tokenArray[i].value.toString() + " which refers to nothing";
                                }
                            } else if (tokenArray[i-1].type == "leftBracket" && tokenArray[i+1].type == "rightBracket") {
                                throw tokenArray[i].value.toString() + " inside the brackets refers to nothing";
                            } else if (tokenArray[i-1].type == "quantity" || tokenArray[i+1].type == "quantity") {
                                throw "check the numeric quantities";
                            }
                            break;
                        case "cdot":
                            if (i == 0) {
                                throw "begins with \u00B7";
                            } else if (i == (tokenArray.length - 1)) {
                                throw "ends with \u00B7";
                            } else if (tokenArray[i-1].type == "cdot" || tokenArray[i+1].type == "cdot") {
                                throw "two adjoining \"\u00B7\"";
                            } else if (brackets != 0) {
                                throw "no \"\u00B7\" inside the brackets allowed";
                            }
                            break;
                        case "leftBracket":
                            brackets += 1;
                            break;
                        case "rightBracket":
                            brackets -= 1;
                            if (brackets < 0) {
                                //a closing bracket without a matching opening one
                                throw "bracket mismatch";
                            } else if (tokenArray[i-1].type == "leftBracket") {
                                throw "nothing inside the brackets";
                            }
                            break;
                        case "element":
                            //could be anywhere, I suppose
                            break;
                        case "unknown":
                        default:
                            throw "\"" + tokenArray[i].value + "\"" + " unrecognized";
                            break;
                    }
                }
                if (brackets != 0) {
                    //probably more opening than closing brackets
                    throw "bracket mismatch";
                }
            } catch(err) {
                errorMsg = "Invalid formula: " + err;
            }
            return errorMsg;
        }

        function makeHTMLformula(tokenArray) {
            //make "beautiful" representation of parsed formula from the token array
            var resultingHTML = "";
            for (var i = 0; i < tokenArray.length; i++) {
                switch (tokenArray[i].type) {
                    case "quantity":
                        //make the number subscript if it is not the first token
                        //and if it is not preceded by either cdot or leftBracket
                        if (i != 0 && tokenArray[i-1].type != "cdot" && tokenArray[i-1].type != "leftBracket") {
                            resultingHTML += "<sub>" + tokenArray[i].value.toString() + "</sub>";
                        } else {
                            resultingHTML += tokenArray[i].value.toString();
                        }
                        break;
                    case "cdot":
                        //make cdot always this one symbol
                        resultingHTML += "\u00B7";
                        break;
                    case "rightBracket":
                    case "leftBracket":
                    case "element":
                        resultingHTML += tokenArray[i].value;
                        break;
                    case "unknown":
                    default:
                        break;
                }
            }
            return resultingHTML;
        }

        function calculateMolarMass(elementsObject) {
            var mass = 0;
            for (var element in elementsObject) {
                mass += periodicTable[element] * elementsObject[element];
            }
            return mass;
        }

        //ACTION!
        //get rid of whitespaces & commas & split on dots (*)
        formula = formula.replace(/\s/g,"");
        formula = formula.replace(/,/g,".");
        //parse and calculate
        var tokens = tokenize(formula);
        var errorMessage = validateFormula(tokens);
        if (errorMessage.length <= 0) {
            this.valid = true;
            this.html = makeHTMLformula(tokens);
            this.elementMoles = countElements(tokens);
            this.molarMass = calculateMolarMass(this.elementMoles);
            if (tokens[0].type == "quantity") {
                this.startsWithNumber = true;
            }
        } else {
            this.html = errorMessage;
            this.valid = false;
        }
    }
    //do not forget to init when the constructor is called
    this.init(this.formula);
}

var LinearSolver = (function() {
    //an oversimplified version of Gauss elimination for solving linear equation systems only
    //inspired by the pseudocode from https://en.wikipedia.org/wiki/Gaussian_elimination
    //and based on an implementation of Ophir LOJKINE https://github.com/lovasoa/linear-solve.git

    function Mat(matrix) {
        //variables
        //mat (matrix) is an array of arrays where mat[i] is a row array
        //it is an augmented matrix with the last column representing the augmentation
        this.mat = [];
        this.invertible;
        this.singular;
        this.rows = matrix.length;
        this.cols = matrix[0].length;

        //initialization
        //creating deep mat array copies
        //can probably be replaced by JSON.parse(JSON.stringify())
        for (var row = 0; row < this.rows; row++) {
            this.mat.push([]);
            for (var col = 0; col < this.cols; col++) {
                this.mat[row].push(matrix[row][col]);
            }
        }

        //methods
        this.swapRows = function(row1, row2) {
            var temp = this.mat[row1];
            this.mat[row1] = this.mat[row2];
            this.mat[row2] = temp;
        }

        this.multiplyRow = function(row, multiplier) {
            for (var col = 0; col < this.cols; col++) {
                this.mat[row][col] *= multiplier;
            }
        }

        this.addRowMultiple = function(destRow, sourceRow, multiplier) {
            for (var col = 0; col < this.cols; col++) {
                this.mat[destRow][col] += this.mat[sourceRow][col] * multiplier;
            }
        }

        this.gauss = function() {
            var nullRows = [];
            //remember - the last column is the augmentation (an answer column)
            for (var col = 0, pivot = 0; col < this.cols - 1; col++, pivot++) {
                var maxValue = 0;
                var maxRow = 0;
                for (var row = pivot; row < this.rows; row++) {
                    //find the row with the maximum value in the current column
                    var val = this.mat[row][col];
                    if (Math.abs(val) > Math.abs(maxValue)) {
                        maxRow = row;
                        maxValue = val;
                    }
                }
                if (maxValue === 0) {
                    //no pivot in this column
                    nullRows.push(pivot);
                } else {
                    this.swapRows(maxRow, pivot);
                    this.multiplyRow(pivot, 1 / maxValue);
                    for (var row = 0; row < this.rows; row++) {
                        if (row !== pivot) {
                            this.addRowMultiple(row, pivot, -this.mat[row][col]);
                        }
                    }
                }
            }
            //if there are columns with no pivots, the matrix is not invertible
            this.invertible = (nullRows.length === 0);
            this.singular = false;
            if (!this.invertible) {
                //to be singular, needs to be noninvertible
                //with nonzero augmentation in zero row
                for (var row = 0; row < nullRows.length; row++) {
                    if (this.mat[row][this.cols - 1] !== 0) {
                        this.singular = true;
                        break;
                    }
                }
            }
        }
    }

    function pseudoGCD(numbers) {
        //floating-point kinda pseudo Greater Common Denominator
        var gcd = undefined;
        var isZero = function(x) { return x === 0; };
        var arr = numbers.map(Math.abs);
        //clear the zeroes
        while (arr.indexOf(0) > -1) {
            arr.splice(arr.indexOf(0), 1);
        }
        if (arr.length >= 2) {
            arr.sort(function(a, b){return a - b});
            gcd = arr[0];
            for (var i = 1; i < arr.length; i++) {
                var rem = gcd;
                while(rem > 1e-8) {
                    gcd = rem;
                    rem = arr[i] % gcd;
                }
            }
        }
        return gcd;
    }

    function solve(augmentedMatrix, needIntegerSolution) {
        //for compatibility with ancient browsers (probably extraneous)
        if (typeof Array.isArray === 'undefined') {
            Array.isArray = function(obj) {
                return Object.prototype.toString.call(obj) === '[object Array]';
            }
        }

        var result = undefined;
        var valid = false;
        //checking the matrix
        if (Array.isArray(augmentedMatrix) && Array.isArray(augmentedMatrix[0])) {
            //checking if the number of columns is the same
            var rows = augmentedMatrix.length;
            if (rows > 1) {
                var columns = augmentedMatrix[0].length;
                for (var row = 1; row < rows; row++) {
                    if (augmentedMatrix[row].length === columns) {
                        valid = true;
                    } else {
                        valid = false;
                        break;
                    }
                }
            } else {
                //a system of one equation can also be valid
                valid = true;
            }
        }

        if (valid) {
            var m = new Mat(augmentedMatrix);
            m.gauss();
            var solution = m.mat.map(function(x) { return x[x.length - 1]; });
            if (needIntegerSolution) {
                var gcd = pseudoGCD(solution);
                if (gcd >= (1/1000)) {
                    //because doesn't work really well with small numbers
                    solution = solution.map(function(x) { return x / gcd; });
                }
            }
            
            result = {
                reduced: m.mat,
                solution: solution,
                invertible: m.invertible,
                singular: m.singular
            };
        }
        
        return result;
    }

    return solve;
})();

function ChemicalEquation(equationText) {
    function Participant(substance, coefficient) {
        this.substance = substance;
        this.coefficient = coefficient;
        this.toString = function() {
            //returns "clear" representation w/o leading 1's and with () around
            //the substances starting with numbers
            var result = "";
            if (Math.abs(this.coefficient - 1) > 1e-8) {
                result += ftoa(this.coefficient);
                if (this.substance.startsWithNumber) {
                    result += "(" + this.substance.html + ")";
                } else {
                    result += this.substance.html;
                }
            } else {
                result += this.substance.html;
            }
            return result;
        }
    }

    this.reagents = [];//an array of participants
    this.products = [];//an array of participants
    this.valid = false;
    this.html = "";

    try {
        if (typeof(equationText) != "string" || equationText.length == 0) {
            throw "empty equation";
        }
        var textParts = equationText.match(chemRegExp("equation"));
        var equalSignIndex = textParts.indexOf("=");
        if (equalSignIndex < 0) {
            throw "No \"=\" sign found";
        }
        if (equalSignIndex == 0) {
            throw "Starts with \"=\" sign";
        }
        if (equalSignIndex == (textParts.length - 1)) {
            throw "Ends with \"=\" sign";
        }
        //equal sign is OK, continue
        textParts.splice(equalSignIndex, 1);
        var substances = [];
        for (var formula of textParts) {
            substances.push(new ChemicalSubstance(formula));
        }
        for (var subst of substances) {
            if (!subst.valid) {
                throw subst.html + " in \"" + subst.formula + "\"";
            }
        }
        //all the substances are OK, let's count our elements...
        var lhsElements = [];
        var rhsElements = [];
        for (var s = 0; s < substances.length; s++) {
            var arrPtr = (s < equalSignIndex) ? lhsElements : rhsElements;
            for (var element in substances[s].elementMoles) {
                if (arrPtr.indexOf(element) < 0) {
                    arrPtr.push(element);
                }
            }
        }
        //...and check if everything's all right with them
        if (lhsElements.length !== rhsElements.length) {
            throw "element mismatch";
        }
        for (var element of lhsElements) {
            if (rhsElements.indexOf(element) < 0) {
                throw "element mismatch";
            }
        }
        //all looks good up to this moment, let's build our matrix and solve
        var equationMatrix = [];
        for (var element of lhsElements) {
            var row = [];
            for (var s = 0; s < substances.length; s++) {
                var quantity = substances[s].elementMoles[element];
                if (typeof(quantity) != "undefined") {
                    quantity = (s < equalSignIndex) ? quantity : -quantity;
                    row.push(quantity);
                } else {
                    row.push(0);
                }
            }
            row.push(0); //do not forget an extra column...
            equationMatrix.push(row);
        }
        //...and an extra row
        var arbitraryRow = [];
        for (var s = 0; s < substances.length + 1; s++) {
            var num = (s === 0 || s === substances.length) ? 1 : 0;
            arbitraryRow.push(num);
        }
        equationMatrix.push(arbitraryRow);
        var solved = LinearSolver(equationMatrix, true);
        if (typeof(solved) == "undefined") {
            throw "cannot solve at all";
        }
        if (solved.singular) {
            throw "cannot solve, singular matrix";
        }
        //the solution seems to exist, filling the reagents and products
        //simultaneously, check for any zero coefficient
        var anyZeroCoefficient = false;
        for (var s = 0; s < substances.length; s++) {
            var coeff = solved.solution[s];
            anyZeroCoefficient |= (coeff === 0);
            var part = new Participant(substances[s], coeff);
            if (s < equalSignIndex) {
                this.reagents.push(part);
            } else {
                this.products.push(part);
            }
        }
        if (anyZeroCoefficient) {
            throw "cannot reach material balance, zero coefficient in the solution";
        }
        //checking the material balance
        for (var element of lhsElements) {
            var balance = 0; //and harmony
            for (var part of this.reagents) {
                var quantity = part.substance.elementMoles[element];
                if (typeof(quantity) != "undefined") {
                    balance += quantity * part.coefficient;
                }
            }
            for (var part of this.products) {
                var quantity = part.substance.elementMoles[element];
                if (typeof(quantity) != "undefined") {
                    balance -= quantity * part.coefficient;
                }
            }
            if (Math.abs(balance) > 1e-8) { //rounding error failsafe
                throw "cannot reach material balance";
            }
        }
        //constructing the html code for the equation with coefficients
        var textArrayOfReagents = [];
        var textArrayOfProducts = [];
        for (var part of this.reagents) {
            textArrayOfReagents.push(part.toString());
        }
        for (var part of this.products) {
            textArrayOfProducts.push(part.toString());
        }
        this.html += textArrayOfReagents.join(" + ") + " = " + textArrayOfProducts.join(" + ");

        //if no error has been thrown, the solution is valid
        this.valid = true;
    } catch (err) {
        this.html = "Invalid equation: " + err;
    }
}

function ftoa(num, precision) {
    //"beautiful" float-to-string conversion
    if (!precision) {
        precision = 6;
    }
    return num.toFixed(precision).replace(/\.?0+$/,"");
}

return {
    periodicTable: periodicTable,
    ChemicalSubstance: ChemicalSubstance,
    ChemicalEquation: ChemicalEquation,
    ftoa: ftoa
};

})();//module end